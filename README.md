# Octo-PS3

Cheap PS3 PSU plus Octopi support in a nice laser cutted wooden box.
Optimized for RepRaps


## Table of Contents

- [Bill of Materials] (#Bill of Materials)
- [Build instructions] (#Build instructions)
- [3D Model sources] (#3D Model sources)
- [Support] (#Support)
- [Contributing] (#Contributing)


## Bill of Materials
| Qty | Component |
| --- | --------- |
| ~0,5m^2 | Plywood 6mm |
| 1 | Raspberry Pi 2/3 |
| 1 | LM2596S based DC-DC step down module |
| 1 | APS-227 PS3 "Fat" PSU (check on ebay the cheapest with the same form factor) |
| 1 | 12V Fan 60mmx25mm |
| 1 Red / 1 Black | Banana 4mm panel connector (keep in mind panel hole is ~11.5mm) |
| 1 | Light Country Rocker Switch DPST 125V 12A (any colour you like) |
| 1 | Rich Bay R-3015 Socket |
| 1 | 2.5A Fast blow Fuse |
| 2 | M3x10mm Pan head Thread-Forming screw (any drive you like) |
| 4 | M4x14mm Flat head ISO metric screw |
| 4 | M4x16mm Socket head ISO metric screw |
| 4 | M4 washers |
| 8 | M4 nuts |
| X | Cables, various colours and sections |

## Build instructions
[Instructables](http://www.instructables.com/) is comin'

## 3D Model sources
The 3D model is hosted on [Onshape](https://cad.onshape.com/documents/1f2efb28b1e294532ad5b9ba/w/ad2f70dfb68f0c4733918eac/e/3c6eafc963638b361aa0d86c)

## Support
Please [open an issue](https://gitlab.com/Crunchlab/Octo-PS3/issues) for support.

## Contributing
Please feel free to fork and open a pull request here or on Onshape :)
